package org.coderead.web.controller;

import lombok.AllArgsConstructor;
import org.coderead.entity.UserTest;
import org.coderead.service.UserTestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/user/")
public class UserTestController {

    private UserTestService userTestService;

    /**
     * 查询所有用户信息
     */
    @GetMapping("list")
    public List<UserTest> test(){

        //直接调用BaseMapper封装好的CRUD方法，就可实现无条件查询数据
        List<UserTest> list = userTestService.list();

        //循环获取用户数据
        for (UserTest userTest:list){
            //获取用户名称
            System.out.println(userTest.getUsername());
        }
        return list;
    }

}
