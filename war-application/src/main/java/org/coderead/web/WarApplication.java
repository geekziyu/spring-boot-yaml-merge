package org.coderead.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("org.coderead")
public class WarApplication {

    public static void main(String[] args) {
        SpringApplication.run(WarApplication.class, args);
    }
}
