package org.coderead.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("org.coderead")
public class BackEndWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackEndWebApplication.class, args);
    }

}
