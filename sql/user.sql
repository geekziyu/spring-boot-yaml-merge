CREATE TABLE `user_test` (
	`id` INT (11),
	`username` VARCHAR (60),
	`age` INT (11),
	`tel` INT (11),
	`create_time` TIMESTAMP ,
	`update_time` TIMESTAMP ,
	`version` INT (11)
);
INSERT INTO `user_test` (`id`, `username`, `age`, `tel`, `create_time`, `update_time`, `version`) VALUES('1','张三','18','180',NULL,NULL,NULL);
INSERT INTO `user_test` (`id`, `username`, `age`, `tel`, `create_time`, `update_time`, `version`) VALUES('2','李四','20','137',NULL,NULL,NULL);
INSERT INTO `user_test` (`id`, `username`, `age`, `tel`, `create_time`, `update_time`, `version`) VALUES('3','王五','22','138',NULL,NULL,NULL);
