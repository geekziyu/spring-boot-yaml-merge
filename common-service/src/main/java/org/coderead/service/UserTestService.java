package org.coderead.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.coderead.dao.UserTestMapper;
import org.coderead.entity.UserTest;
import org.springframework.stereotype.Service;

@Service
public class UserTestService extends ServiceImpl<UserTestMapper, UserTest> {

}
