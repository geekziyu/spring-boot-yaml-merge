package org.coderead.entity;

import lombok.Data;

import java.util.Date;

@Data	//使用lombok 简化getset方法
public class UserTest {

    private int id;

    private String username;

    private int age;

    private int tel;

    private Date create_time;

    private Date update_time;

    private int version;
}
