package org.coderead.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("org.coderead.dao")
public class MyBatisPlusConfig {

}
