package org.coderead.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.coderead.entity.UserTest;

public interface UserTestMapper extends BaseMapper<UserTest> {
}
