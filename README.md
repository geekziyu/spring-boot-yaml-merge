# spring-boot-yaml-merge

#### 介绍
SpringBoot配置文件拆分设计思路

#### 安装教程
1. 请自行搭建 MySQL 数据库，创建一个数据库并执行 sql/user.sql 初始化表；

#### 使用说明
1. 启动`BackEndWebApplication`  后请访问 `http://localhost:8083/user/list`